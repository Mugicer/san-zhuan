﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Goods : MonoBehaviour {
    [Header("玩家資料")]
    public player_input pi;
    public player_output po;
    public bulletcreater1 bc;
    [Header("商品數據")]
    public GameObject obj;
    public Sprite image;
    public int cost;
    public int nowlevel;
    public int Maxlevel;
    [Header("商品架")]
    public Text costtime;
    public Text number;
    public Image pic_position;
    public bool canbuy;


	// Use this for initialization
	void Start () {
        pi = GameObject.FindGameObjectWithTag("Player").GetComponent<player_input>();
        po = GameObject.FindGameObjectWithTag("Player").GetComponent<player_output>();
        costtime = transform.GetChild(0).GetChild(0).GetChild(1).GetComponent<Text>();
        number = transform.GetChild(0).GetChild(4).GetComponent<Text>();
        pic_position = transform.GetChild(0).GetChild(2).GetComponent<Image>();
    }
	
	// Update is called once per frame
	void Update () {
        if (image!=null)
        {
            pic_position.sprite = image;
        }
        costtime.text = "消耗" + cost + "秒";
        number.text = nowlevel + "/" + Maxlevel;
        if (nowlevel==Maxlevel)//購買按鍵取消
        {
            transform.GetChild(0).GetChild(0).GetComponent<Button>().interactable = false;
        }

    }
    public void buybtn() {
        nowlevel++;
        po.LifeOfTime -= cost;
    }
    public void plusMaxHP(){
        GetComponent<BarControl>().changeMaxHP(100+nowlevel*50);
    }
    public void plusDamage(){
        GetComponent<player_output>().str = 2 + (nowlevel * 2);
    }
    public void plusSpeed(){
        GetComponent<player_output>().movespeed = 900 + (nowlevel * 30);
    }
    public void plusMaxMP(){
        GetComponent<BarControl>().changeMaxMP(100 + nowlevel * 30);
    }
    public void AddLightBullet(){
        bc.AddBullet(obj);
    }
}
