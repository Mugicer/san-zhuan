﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Real_Bat : MonoBehaviour {
    private Rigidbody rb;
    public bullet bu;
    public player_output po;
    public int paremeter = 1;
    public int attack = (-10);
    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        bu = transform.parent.gameObject.GetComponent<bullet>();
        po = GameObject.FindGameObjectWithTag("Player").GetComponent<player_output>();
        paremeter = transform.parent.GetComponent<bullet>().paremeter;
    }

    // Update is called once per frame
    void Update()
    {
        attack = (po.str) * -1 * paremeter;
        transform.Rotate(1f * Time.timeScale, 2f * Time.timeScale, 5f * Time.timeScale);
    }
    public void OnCollisionEnter(Collision col)
    {
        Debug.Log(col);
        col.transform.SendMessage("plusHP", attack, SendMessageOptions.DontRequireReceiver);
        if (col.gameObject.tag=="boss")
        {
            po.GetComponent<BarControl>().plusHP(attack * (-1)/2);
            Debug.Log("吸血");
        }
        Destroy(bu.gameObject);
    }
}
