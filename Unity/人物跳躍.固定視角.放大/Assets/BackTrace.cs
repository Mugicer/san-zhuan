﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackTrace : MonoBehaviour {
    private player_output po;
    private player_input pi;
    public float time = 4;
    public Vector3 playerposition;
    private int str = 1;
    private float movespeed = 900;//移速
    private int MaxHitPoint = 999;
    private int HitPoint = 999;
    private int MaxManaPoint = 100;
    private int ManaPoint = 100;
    private void Awake()
    {
        po = GetComponent<player_output>();
        pi = GetComponent<player_input>();
    }
    // Update is called once per frame
    void Update () {

    }
    void DataUpDate() {

        playerposition = transform.position;
        str            = po.str;
        movespeed      = po.movespeed;
        MaxHitPoint    = po.MaxHitPoint;
        HitPoint       = po.HitPoint;
        MaxManaPoint   = po.MaxManaPoint;
        ManaPoint      = po.ManaPoint;     
    }

    void TraceBack()
    {
       transform.position  = playerposition ;
       po.str              = str            ;
       po.movespeed        = movespeed      ;
       po.MaxHitPoint      = MaxHitPoint    ;
       po.HitPoint         = HitPoint       ;
       po.MaxManaPoint     = MaxManaPoint   ;
       po.ManaPoint        = ManaPoint      ;
    }
}
