﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class puppter_teleport : StateMachineBehaviour {
    public float TPTimeafter=1;
    public float range = 2;
    private bool TPafter = true;
    public bool backtodetect = false;
    private Transform player;
    private puppeteer puppeteer;
    public GameObject[] pet;
    private float time;
	 // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        puppeteer = animator.GetComponent<puppeteer>();
        player = puppeteer.player.transform;
        for (int i = 0; i < 5; i++)
        {
            pet[i] = puppeteer.petarr[i];
        }
        puppeteer.numcount = 0;
        puppeteer.transform.position = player.forward * -4+new Vector3(player.position.x,puppeteer.transform.position.y,player.position.z);
        puppeteer.transform.LookAt(new Vector3(player.position.x, puppeteer.transform.position.y, player.position.z));
        time = Time.time;
	}

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        if (Time.time - time>TPTimeafter&&TPafter)
        {
            animator.SetBool("Action", true);
            TPafter = false;
        }
	}

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        TPafter = true;
        animator.SetBool("teleport", false);

    }

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
