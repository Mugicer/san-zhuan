﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Animator))]
public class AnimatorOverride : MonoBehaviour {
    public Animator Anim;
    // Use this for initialization
    void Start () {
        Anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void OnAnimatorMove() { }//純覆蓋用
}
