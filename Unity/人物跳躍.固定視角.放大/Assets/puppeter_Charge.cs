﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class puppeter_Charge : StateMachineBehaviour {
    public puppet[] pet = new puppet[5];
    private bool a = true, b = true, c = true;
     // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        animator.GetComponent<puppeteer>().numcount=0;
        for (int i = 0; i < 5; i++)
        {
            pet[i] = animator.GetComponent<puppeteer>().petarr[i].GetComponent<puppet>();
        }
    }

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        if (a)
        {
            pet[0].anim.SetBool("gethigh", true);
            pet[1].anim.SetBool("gethigh", true);
            a = false;
        }
        if (animator.GetComponent<puppeteer>().numcount>=2)
        {
            if (b)
            {
                pet[2].anim.SetBool("TP", true);
                pet[3].anim.SetBool("TP", true);
                b = false;
            }
            if (animator.GetComponent<puppeteer>().numcount >= 4)
            {
                if (c)
                {
                    pet[4].anim.SetBool("charge3",true);
                    c = false;
                }

                if (animator.GetComponent<puppeteer>().numcount >= 5)
                {
                    animator.GetComponent<puppeteer>().numcount = 0;
                    animator.SetBool("Action", true);
                    Debug.Log("detect");

                }
            }

        }
       

    }

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        animator.SetBool("Charge", false);
        a = true;
        b = true;
        c = true;
	}

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
