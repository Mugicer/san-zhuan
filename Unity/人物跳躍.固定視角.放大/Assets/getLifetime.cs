﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class getLifetime : MonoBehaviour {
    public Text t;
    public player_output po;
	// Use this for initialization
	void Start () {
        t = GetComponent<Text>();
        po =GameObject.FindGameObjectWithTag("Player").GetComponent<player_output>();
	}
	
	// Update is called once per frame
	void Update () {
        t.text = ((int)po.LifeOfTime).ToString();
	}
}
