﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserBoss_LaserEyeLine : StateMachineBehaviour {
    public GameObject Lock;
    public GameObject boom;
    public float maxDistance;
    private Transform player;
    private Quaternion bossrotation;
    private Transform laserBoss;
    private Vector3 start;
    private Vector3 way;
    private Vector3 end;
    private RaycastHit Hit;
    private float rotate;
    private bool step1;
    private bool step2;
    bool hitthings;
    public float rotateSpeed;
    public float waitsecond;
    private float starttime;
    private GameObject a;
    private GameObject b;
	 // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        animator.SetBool("EyeLine",true);
        rotateSpeed = animator.GetFloat("EyeLineSpeed");
        waitsecond = animator.GetFloat("EyeLineTime");
        maxDistance = animator.GetFloat("EyeLineLong");
        laserBoss = animator.transform;
        player = laserBoss.gameObject.GetComponent<LaserBoss>().player.transform;
        start = laserBoss.position;
        laserBoss.LookAt(new Vector3(player.position.x, laserBoss.position.y, player.position.z));
        bossrotation = laserBoss.rotation;
        Debug.Log("bossrotation" + bossrotation);
        laserBoss.LookAt(new Vector3(player.position.x + Random.Range(0,3f),laserBoss.position.y , player.position.z + Random.Range(0, 3f)));
        Debug.Log(new Vector3(player.position.x + Random.Range(0, 3f), laserBoss.position.y, player.position.z + Random.Range(0, 3f)));

        way = laserBoss.forward;
        hitthings = Physics.Raycast(start, way, out Hit, maxDistance);
        if (hitthings)
        {
            end = Hit.point;
        }
        else
        {
            end = start + way * maxDistance;
        }
        a = Instantiate(Lock,laserBoss);
        a.GetComponent<SingleLineLock>().setLine(start,end);
        if (bossrotation.y>laserBoss.rotation.y)
        {
            rotate = -1;
        }
        else
        {
            rotate = 1;
        }
        step1 = true;
        step2 = false;
        starttime = Time.time;
    }

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        if (Time.time-starttime>waitsecond)
        {
            step2 = true;
        }
        if (Time.time-starttime>animator.GetFloat("EyeLineEndTime"))
        {
            animator.SetBool("EyeLine",false);
        }
        if (step1)
        {
            if (step2)
            {
                Destroy(a);
                b = Instantiate(boom,laserBoss.position,laserBoss.rotation,laserBoss);
                step1 = false;
            }
        }
        else
        {
            laserBoss.Rotate(0, rotate * Time.deltaTime * rotateSpeed, 0);
            way = laserBoss.forward;
            hitthings = Physics.Raycast(start, way, out Hit, maxDistance);
            if (hitthings)
            {
                end = Hit.point;
            }
            else
            {
                end = start + way * maxDistance;
            }

            b.GetComponent<LaserLine>().setCapsule(start, end);
            b.GetComponent<LaserLine>().setLine(start, end);
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        if (a != null)
        {
            Destroy(a);
        }
        if (b != null)
        {
            Destroy(b);
        }
    }

    // OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}
}
