﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class puppet_TP : StateMachineBehaviour {
    public GameObject ball;
    private Transform player;
    private puppeteer puppeteer;
    private Vector3 v;
    private float time;
    public float waittime = 1f;
    private bool a = true, b = true;
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        player = animator.GetComponent<puppet>().player.transform;
        puppeteer = animator.GetComponent<puppet>().puppeteer;
        if (animator.GetComponent<puppet>().num == 2)
        {
            v = player.transform.position + puppeteer.transform.right * 2 + new Vector3(0, 1, 0);
            //Instantiate(ball,v, animator.transform.rotation);
        }
        if (animator.GetComponent<puppet>().num == 3)
        {
            v = player.transform.position + puppeteer.transform.right * 2 * (-1) + new Vector3(0, 1, 0);
            //Instantiate(ball, v, animator.transform.rotation);
        }
        time = Time.time;
    }

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        if (Time.time - time > waittime)
        {
            animator.SetBool("attack", true);
        }
        else
        {
            if (animator.GetComponent<puppet>().num == 2 )
            {
                animator.GetComponent<puppet>().transform.position = v;
            }
            if (animator.GetComponent<puppet>().num == 3 )
            {
                animator.GetComponent<puppet>().transform.position = v;
            }
            animator.GetComponent<puppet>().Look();
        }
    }

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        animator.SetBool("TP", false);
        a = true;
        b = true;
	}

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
