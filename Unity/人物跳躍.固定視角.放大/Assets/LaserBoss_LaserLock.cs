﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserBoss_LaserLock : StateMachineBehaviour {
    GameObject LaserLock;
    LaserBoss self;
    public GameObject[] obj = new GameObject[2];
    Transform target ;
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        self = animator.GetComponent<LaserBoss>();
        target = self.taragets[0].transform;
        LaserLock = self.Lasers[1];
        animator.SetBool("LaserLock", true);
        //animator.SetBool("Again", true);
    }

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        if (obj[0] == null /*&& obj[1] == null*/) {
            target = self.taragets[Random.Range(0,7)].transform;
            if (animator.GetInteger("LockTime") < 7)
            {
                self.transform.LookAt(new Vector3(target.position.x, self.transform.position.y, target.position.z));//水平看目標 
                obj[0] = Instantiate(LaserLock, self.bigeye.transform.position, self.bigeye.transform.rotation);//創造射線
                animator.GetComponent<LaserBoss>().LaserOnWorld[0] = obj[0];
                Destroy(obj[0],3);
                animator.SetInteger("LockTime", animator.GetInteger("LockTime") + 1);//tracetime次數++
            }
            else
            {

                //animator.SetBool("Again", false);
                
                animator.SetBool("LaserLock", false);//決定本狀態結束
            }
        }
        
    }

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        animator.SetInteger("LockTime", 0);//參數歸零
    }

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
