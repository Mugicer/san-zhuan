﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TheGod : MonoBehaviour {
    public GameObject store;
    public GameObject talkplace;
    public GameObject choose;
    public GameObject talktext;
    private Animator anim;
    public StoreData Gm;
    public int TPnumber = 1;
    public string[] words = new string[3]{"a","b","c"};


    public int[] TPscence = new int[2] {3,5};
	void Start () {
        anim = transform.GetComponent<Animator>();
	}
	
	void Update () {
        if (StoreData.Instance.eye)
        {
            TPnumber = 5;
            if (StoreData.Instance.women)
            {
                TPnumber =7;
            }
        }
	}
    public void askGod() {
        anim.SetBool("ask",true);
    }
    public void btntalk() {
        anim.SetBool("talk", true);
    }
    public void btnstore() {
        anim.SetBool("openstore", true);
    }
    public void btnTP() {
        anim.SetBool("TP", true);
    }
    public void btnQuit() {
        anim.SetBool("quit", true);
    }
        public void Openwords() {
            talktext.SetActive(true);
        }
        public void Closewords()
        {
            talktext.SetActive(false);
        }
    public void Openplace() {
        talkplace.SetActive(true);
    }
    public void Closeplace() {
        talkplace.SetActive(false);
    }
        public void Openchoose()
        {
            choose.SetActive(true);
        }
        public void Closechoose()
        {
            choose.SetActive(false);
        }
    public void OpenStore() {
        store.SetActive(true);
    }
    public void CloseStore() {
        store.SetActive(false);
    }


    public void TP() {
        SceneManager.LoadScene(TPnumber);
    }
    public void Quit() {
        if (store.active)
        {
            CloseStore();
        }
        if (talkplace.active)
        {
            Closeplace();
        }
        if (choose.active)
        {
            Closechoose();
        }
        if (talktext.active)
        {
            Closewords();
        }
    }
}
