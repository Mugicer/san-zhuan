﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserLock : MonoBehaviour {
    public GameObject Laserboom;
    private Laserboom b;
    private RaycastHit Hit;
    Vector3 start;
    Vector3 way;
    Vector3[] points;
    public LineRenderer lineRend;
    public float linewidth;
    public float Hitpoint;
    public float boomseconds;
    float count;
    // Use this for initialization
    private void Awake()
    {
        lineRend = GetComponent<LineRenderer>();
        b = Laserboom.GetComponent<Laserboom>();
        points = new Vector3[(int)Hitpoint];
        lineRend.positionCount=(int)Hitpoint;
    }
    void Start()
    {
        start = transform.position;
        way = transform.forward;
        lineRend.SetWidth(linewidth, linewidth);
        points[0] = transform.position;
        lineRend.SetPosition(0, transform.position);
        for (count=1; count<Hitpoint; count++) {
            caculate();
            points[(int)count] = Hit.point;
            
            lineRend.SetPosition((int)count, Hit.point);
        }
        StartCoroutine(boom());
    }
    IEnumerator boom() {
        yield return new WaitForSeconds(boomseconds);
        for (int i = 1; i < Hitpoint; i++)
        {
            //Debug.Log(i);
            GameObject a= Instantiate(Laserboom,this.transform);
            a.GetComponent<Laserboom>().setLine(i,points[i-1],points[i]);
        }
        lineRend.SetWidth(1, 1);
    }

    void caculate()
    {
        Physics.Raycast(start, way, out Hit);
        start = Hit.point;//計算新向量
        way = Hit.normal * 2 + way;
    }
    float getLineWidth() {
        return linewidth;
    }
}
