﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckStartBlock : MonoBehaviour {
    public GameObject boss;
    public GameObject door;
	// Use this for initialization
	void Start () {
        boss = GameObject.FindGameObjectWithTag("boss");
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag=="Player")
        {
            boss.SendMessage("start",SendMessageOptions.DontRequireReceiver);
            door.SetActive(true);
        }
        
    }
}
