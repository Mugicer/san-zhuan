﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserBoss_LaserAround : StateMachineBehaviour {
    public CreatVerticalLine LaserAround;
    public float distance;
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        LaserAround = animator.GetComponentInChildren<CreatVerticalLine>();
        animator.SetBool("LaserAround", true);
        LaserAround.SetNumber(animator.GetInteger("AroundNumber"));
        Debug.Log("00");
        LaserAround.SetDistance(animator.GetFloat("AroundDistance"));
        distance = animator.GetFloat("AroundDistance");
        LaserAround.CreatLines();
	}

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        distance = distance + Time.deltaTime*animator.GetFloat("AroundSpeed") ;
        LaserAround.SetDistance(distance);
        if (distance>=animator.GetFloat("MaxDistance"))
        {
            animator.SetBool("LaserAround",false);
        }
    }

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        LaserAround.ClearLines();
        distance = 0;
        
    }

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
