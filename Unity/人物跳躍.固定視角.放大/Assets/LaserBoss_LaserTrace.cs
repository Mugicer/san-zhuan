﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserBoss_LaserTrace : StateMachineBehaviour {
    GameObject LaserTrace;
    LaserBoss self;
    int b;
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        self = animator.GetComponent<LaserBoss>();
        LaserTrace = self.Lasers[2];
        self.transform.LookAt(self.player.transform);
        animator.SetBool("LaserTrace", true);

        b = Random.Range(0, 2);
        GameObject trace = Instantiate(LaserTrace, self.handeyes[b].transform.position, self.handeyes[b].transform.rotation);
        for (int i = 0; i < self.LaserTracing.Length; i++)
        {
            if (self.LaserTracing[i] == null) {
                self.LaserTracing[i] = trace;
                Destroy(trace, 20f);
                trace = null;
            }
        }
        
        if (animator.GetInteger("TraceTime") < 2)
        {
            animator.SetInteger("TraceTime", animator.GetInteger("TraceTime") + 1);//tracetime++
        }
        else
        {
            animator.SetBool("LaserTrace", false);
            animator.SetInteger("TraceTime", 0);
        }
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        self.transform.LookAt(self.player.transform);
    }

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        
        
    }

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
