﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserLine_Keep : MonoBehaviour {
    RaycastHit Hit;
    Vector3 start;
    Vector3 end;
    LaserLine laserLine;
	// Use this for initialization
	void Start () {
        laserLine = GetComponent<LaserLine>();
        Physics.Raycast(this.transform.position, Vector3.up, out Hit);
        end = Hit.point;
        Physics.Raycast(this.transform.position, Vector3.down, out Hit);
        start = Hit.point;
        transform.position = start+new Vector3(0,0.01f,0);
    }
	
	// Update is called once per frame
	void Update () {
        Physics.Raycast(this.transform.position, Vector3.up, out Hit);
        end = Hit.point;
        Physics.Raycast(this.transform.position, Vector3.down, out Hit);
        start = Hit.point;
        laserLine.setLine(start, end);
        laserLine.setCapsule(start, end);
    }
    
}
