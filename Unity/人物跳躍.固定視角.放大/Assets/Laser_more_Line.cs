﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser_more_Line : MonoBehaviour {
    public GameObject Laserbody;
    public RaycastHit Hit;
    public Vector3 start;
    public Vector3 way;
    public Vector3 end;
    private LineRenderer lineRend;
    public float LineWidth;
    public float waitsecond;

    public bool gohead = true;
    public bool gotail = false;
    public int Number_Of_point;
    public int now_Of_Point;
    private Vector3[] LaserPoint;
    private GameObject obj;
    private GameObject[] objbody;
    public float Laserspeed = 0.1F;
    private float speed;
    public int damage = 1;
    public float HitTimes;
    public float firstmove;
    float count;
    // Use this for initialization
    private void Awake()
    {
        lineRend = GetComponent<LineRenderer>();
        LaserPoint = new Vector3[Number_Of_point];
        objbody = new GameObject[Number_Of_point-1];
        now_Of_Point = 1;
    }
    void Start()
    {
        LaserPoint[0] = transform.position;
        lineRend.SetPosition(0,transform.position);
        start = transform.position;
        way = transform.forward;
        end = transform.position;
        
        speed = Laserspeed;
        Physics.Raycast(start, way, out Hit);//計算第一段路徑
        firstmove = Hit.distance / Laserspeed;
        LaserPoint[1] = Hit.point;
        obj = Instantiate(Laserbody, transform.position, transform.rotation, transform);
        objbody[0] = obj;
        obj.GetComponent<Laserboom>().setLine(now_Of_Point, start, end);//加第一段雷射實體
    }
    void Update()
    {
        if (gohead)
        {
            move();
            if (now_Of_Point+1==Number_Of_point&&end==Hit.point) {
                gohead = false;

                now_Of_Point = 0;
                end = LaserPoint[0];
                speed = Laserspeed;//重置速度
                firstmove = Vector3.Distance(LaserPoint[0],LaserPoint[1]) / Laserspeed;

                StartCoroutine(waittail(waitsecond*2));//等待 waitsecond 接著 gotail=true
            }
            else if (end==Hit.point)
            {
                gohead = false;
                start = Hit.point;
                way = Vector3.Reflect(way,Hit.normal);
                Physics.Raycast(start, way, out Hit);

                StartCoroutine(waithead(waitsecond));//等待 waitsecond 接著 gohead=true

                LaserPoint[now_Of_Point+1] = Hit.point;
                lineRend.positionCount++;
                obj=Instantiate(Laserbody,transform.position,transform.rotation,transform);//創造新的體積
                objbody[now_Of_Point]=obj;//紀錄obj
                obj.GetComponent<Laserboom>().setLine(now_Of_Point,start,start);//本段雷射原點
                now_Of_Point++;
                lineRend.SetPosition(now_Of_Point, end);//將最新的雷射點從0,0,0 改至當下

                speed = Laserspeed;//重置速度
                firstmove = Hit.distance / Laserspeed;
            }
        }
        if (gotail)
        {
            end = Vector3.Lerp(end, LaserPoint[now_Of_Point+1], speed);
            speed = (firstmove / Vector3.Distance(end, LaserPoint[now_Of_Point + 1])) * Time.deltaTime ;

            for (int i = 0; i <= now_Of_Point; i++)
            {
                lineRend.SetPosition(i, end);
            }
            if (now_Of_Point+1<LaserPoint.Length)
            {
                objbody[now_Of_Point].GetComponent<Laserboom>().setLine(now_Of_Point, end, LaserPoint[now_Of_Point + 1]);
            }
            if (now_Of_Point+1==Number_Of_point-1 && end== LaserPoint[now_Of_Point + 1])
            {
                Destroy(this.gameObject);
            }
            else if (end == LaserPoint[now_Of_Point + 1])
            {
                gotail = false;

                now_Of_Point++;
                end = LaserPoint[now_Of_Point];//重設尾巴

                Destroy(objbody[now_Of_Point-1]);//銷毀碰撞體

                speed = Laserspeed;//重置速度
                firstmove = (LaserPoint[now_Of_Point] - LaserPoint[now_Of_Point + 1]).magnitude / Laserspeed;

                gotail = true;
            }
        }

    }
    IEnumerator waithead(float t) {
        yield return new WaitForSeconds(t);
        gohead = true;
    }
    IEnumerator waittail(float t)
    {
        yield return new WaitForSeconds(t);
        gotail = true;
    }
    private void move()
    {
        end = Vector3.Lerp(end, Hit.point, speed);
        //Debug.Log(point0);
        speed = (firstmove / Vector3.Distance(end, Hit.point)) * Time.deltaTime;
        lineRend.SetPosition(now_Of_Point, end);
        obj.GetComponent<Laserboom>().setLine(now_Of_Point, start, end);
    }
    
}
