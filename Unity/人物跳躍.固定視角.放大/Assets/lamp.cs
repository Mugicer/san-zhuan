﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class lamp : MonoBehaviour {
    public int hp;
    public float lifelimit;
    public GameObject explosion;
    float time;
	// Use this for initialization
	void Start () {
        time = Time.time;
    }
	
	// Update is called once per frame
	void Update () {
        
        if (lifelimit < Time.time - time)
        {
            GameObject a=Instantiate(explosion,transform);
            a.transform.parent = null;
            Debug.Log("timelimit");
            Destroy(gameObject);
        }
        if (hp<0)
        {
            Destroy(gameObject);
        }
    }
    void plusHP(int a) {
        hp += a;
    }
}
