﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlindWomen_MixAttack : StateMachineBehaviour {
    public GameObject TruthBullet;
    public GameObject FakeBullet;
    public GameObject lamp;
   
    public float creatdistance = 1;
    public float creatforsecond = 1;
    public GameObject[] po;
    float time =0;
    float[] ro = new float[] { 0, 15,-15, 30, -30, 45, -45 }; 
    bool shoot = true;
    GameObject a;
    GameObject b;
    GameObject child;
    GameObject mid;
    Transform player;
	 // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        a = animator.GetComponent<Blind_women>().empty;
        b = animator.GetComponent<Blind_women>().childrenempty;
        child = animator.GetComponent<Blind_women>().children;
        mid = animator.GetComponent<Blind_women>().mid;
        player = animator.GetComponent<Blind_women>().player.transform;
        shoot = true;
        for (int i = 0; i < 4; i++)
        {
            po[i] = animator.GetComponent<Blind_women>().po[i];
        }
        //還需要設置 盲女 位置
        animator.transform.position = po[Random.Range(0,4)].transform.position + new Vector3(Random.Range(5f, -5f), 0, Random.Range(5f,-5f));
        //還需要設置 小女孩 位置
        child.transform.position = mid.transform.position + new Vector3(Random.Range(35f,-35f),0,Random.Range(27.5f,-27.5f));
        for (int i = 0; i < 3; i++)
        {
           
            GameObject c = Instantiate(lamp);
            animator.GetComponent<Blind_women>().Lamp[i]=c;
            c.transform.position = mid.transform.position + new Vector3(Random.Range(35f, -35f), 0, Random.Range(27.5f, -27.5f));
        }
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        animator.transform.LookAt(new Vector3(player.position.x,animator.transform.position.y, player.position.z));
        child.transform.LookAt(new Vector3(player.position.x, animator.transform.position.y, player.position.z));
        if (shoot)
        {
            for (int i = 0; i < ro.Length; i++)
            {

                a.transform.Rotate(0,ro[i],0);
                a.transform.Translate(0,1,creatdistance);
                Instantiate(FakeBullet, a.transform.position, a.transform.rotation);
                a.transform.SetPositionAndRotation(animator.transform.position,animator.transform.rotation);
            }
            for (int i = 0; i < ro.Length; i++)
            {

                b.transform.Rotate(0, ro[i], 0);
                b.transform.Translate(0, 1, creatdistance);
                Instantiate(TruthBullet, b.transform.position, b.transform.rotation);
                b.transform.SetPositionAndRotation(child.transform.position, child.transform.rotation);
            }
            shoot = false;
        }
        time += Time.deltaTime;
        if (time>creatforsecond)
        {
            time -= creatforsecond;
            shoot = true;
        }
    }

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        
	}

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
