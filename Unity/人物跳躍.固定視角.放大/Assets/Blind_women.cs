﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blind_women : MonoBehaviour {
    public Animator anim;
    public GameObject player;
    public GameObject HPbar;
    public GameObject model;
    public GameObject BulletCreater;
    public GameObject TheLight;
    public GameObject playerlight;
    public GameObject[] Lamp;
    bool LampPrepare = false;
    public GameObject mid;
    public int Lampcount;
    public GameObject children;
    public GameObject empty;
    public GameObject childrenempty;
    public GameObject[] po;
    public GameObject door;

    // Use this for initialization
    [Header("數據")]
    public int HP = 999;
    public int MAXHP = 999;

    public GameObject treture;
    private float lightstart = 0;
    public float lighttime = 0;
    

    private void Awake()
    {
        player = GameObject.FindWithTag("Player");
        HPbar.GetComponent<Bar>().Max = MAXHP;
        HPbar.GetComponent<Bar>().now = HP;
        anim = model.GetComponent<Animator>();
    }
    void Start () {

        lightstart = Time.time;
        if (StoreData.Instance.women)
        {
            plusHP(-1 * MAXHP);
        }
    }
	
	// Update is called once per frame
	void Update () {
        if (anim.GetBool("light"))
        {
            lighttime = Time.time - lightstart;
        }  
        animParameter();
    }


    private void animParameter()
    {
        anim.SetFloat("LightTime", lighttime);
        //Debug.Log(lighttime);
        anim.SetFloat("hp", HP);
        HPbar.GetComponent<Bar>().now = HP;
        if (anim.GetFloat("LightTime") > 30)
        {
            lightstart = Time.time;
            lighttime = 0;
            TheLight.transform.Rotate(new Vector3(-180, 0, 0));
            TheLight.GetComponent<Light>().cullingMask = 256;
            anim.SetFloat("LightTime", lighttime);
            anim.SetBool("light", false);
            playerlight.SetActive(true);
        }                                           //刷新光照時間&關燈
        if (Lamp[2]!=null)
        {
            LampPrepare = true;
        }
        if (!anim.GetBool("light")&&LampPrepare)  //偵測燈光
        {
            Lampcount = 3;
            if (Lamp[0]==null)
            {
                Lampcount--;
            }
            if (Lamp[1] == null)
            {
                Lampcount--;
            }
            if (Lamp[2] == null)
            {
                Lampcount--;
            }
            if (Lampcount == 3)
            {
                playerlight.GetComponent<Player_Light>().range = 15;
            }
            if (Lampcount == 2)
            {
                playerlight.GetComponent<Player_Light>().range = 20;
            }
            if (Lampcount == 1)
            {
                playerlight.GetComponent<Player_Light>().range = 30;
            }
            if (Lampcount==0)
            {
                TheLight.transform.Rotate(new Vector3(-180, 0, 0));
                TheLight.GetComponent<Light>().cullingMask = -1;
                playerlight.SetActive(false);
                anim.SetBool("light", true);
                lightstart = Time.time;
                LampPrepare = false;
            }
        }
        if ( (HP*10)/MAXHP < 3)//狂暴
        {
            //Debug.Log(HP / MAXHP);
            anim.SetBool("crazy", true);
        }
        if (HP<0)
        {
            Dead();
        }
    }

    public void plusHP(float a)
    {
        HP = HP + (int)a;
    }
    public void start() {
        anim.SetBool("Start", true);
    }
    public void Dead()
    {
        if (treture != null)
        {
            treture.SetActive(true);
        }
        Destroy(door);
        if (!anim.GetBool("light"))
        {
            TheLight.transform.Rotate(new Vector3(180, 0, 0));
            for (int i = 0; i < 3; i++)
            {
                Destroy(Lamp[i]);
            }
        }
        StoreData.Instance.women = true;
        Destroy(children);
        Destroy(this.gameObject);
    }
}
