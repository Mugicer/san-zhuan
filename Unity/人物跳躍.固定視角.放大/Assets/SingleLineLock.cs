﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingleLineLock : MonoBehaviour {
    public LineRenderer lineRenderer;
    public float LineWidth;
    public Vector3 point0;
    public Vector3 point1;
    bool use = true;
    private RaycastHit Hit;
    // Use this for initialization
    private void Awake()
    {
        lineRenderer = GetComponent<LineRenderer>();
    }
    void Start()
    {
        LineWidth = lineRenderer.startWidth;
        Physics.Raycast(transform.position, transform.forward, out Hit);
        point0 = transform.position;
        point1 = Hit.point;
    }

    // Update is called once per frame
    void Update()
    {
        lineRenderer.SetPosition(0, point0);
        lineRenderer.SetPosition(1, point1);
    }
    public void setLine(Vector3 start, Vector3 end)
    {
        point0 = start;
        point1 = end;
    }
}
