﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlindWomen_EraseLight : MonoBehaviour {
    public void OnTriggerEnter(Collider other)
    {
        if (!other.GetComponent<ShadowBullet_Real>().istrue)
        {
            other.SendMessage("Delete");
            Debug.Log(1);
        }
        Debug.Log(other);
    }
}
