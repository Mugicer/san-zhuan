﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlTime : MonoBehaviour {
    public player_input pi;
    public player_output po;
	// Use this for initialization
	void Start () {
        pi = GetComponent<player_input>();
        po = GetComponent<player_output>();
	}
	
	// Update is called once per frame
	void Update () {
        if (pi.UIControling)
        {

        }
        else
        {
            if (po.LifeOfTime > 0)
            {
                po.LifeOfTime -= 1 * Time.deltaTime;
            }
        }
    }
}
