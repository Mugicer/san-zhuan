﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TeleportGate : MonoBehaviour {
    public int number;
    public void OnTriggerStay(Collider other)
    {
        if (other.tag=="Player"&&other.GetComponent<player_input>().talk)
        {
            other.SendMessage("SaveData");

            SceneManager.LoadScene(number);
        }
    }
}
