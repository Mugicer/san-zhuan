﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShadowBullet_Real : MonoBehaviour {
    public int attack = (-10);
    public float speed = 1;
    public bool istrue = true;
    private void Start()
    {
        Destroy(gameObject, 10);
    }
    private void Update()
    {
        transform.Translate(new Vector3(0,0,1)*speed*Time.deltaTime);
    }
    public void OnTriggerEnter(Collider other)
    {
        if (other.tag=="Player")
        {
            other.transform.SendMessage("plusHP", attack, SendMessageOptions.DontRequireReceiver);
            Destroy(gameObject);
        }
        //Debug.Log(other);
        if (other.tag=="wall")
        {
        Destroy(gameObject);
        }
    }
    public void Delete() {
        if (istrue)
        {
            Destroy(gameObject);
        }
    }
}
