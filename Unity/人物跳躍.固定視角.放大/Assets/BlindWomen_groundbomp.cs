﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlindWomen_groundbomp : StateMachineBehaviour {
    public bool Creatbump;
    public GameObject bump;
    public GameObject bump2;
    public GameObject mid;
    public float rg;
    float time;
	 // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        mid = animator.GetComponent<Blind_women>().mid;
	}

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        if (Creatbump)//爆炸
        {
            Instantiate(bump, mid.transform.position + new Vector3(Random.Range(-rg, rg), 0, Random.Range(-rg, rg)), new Quaternion());
            Instantiate(bump2, mid.transform.position + new Vector3(Random.Range(-rg, rg), 0, Random.Range(-rg, rg)), new Quaternion());
            Creatbump =false;
        }
        if (time>animator.GetFloat("bumptime"))//間隔時間
        {
            time -= animator.GetFloat("bumptime");
            Creatbump = true;
        }
        time += Time.deltaTime;
	}

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	//override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
