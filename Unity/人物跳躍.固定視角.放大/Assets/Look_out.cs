﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Look_out : MonoBehaviour {
    private Vector3 target;
    // Use this for initialization
    private void Awake()
    {
        target =  this.transform.position - this.transform.parent.transform.position;
        this.transform.forward = target;
    }
}
