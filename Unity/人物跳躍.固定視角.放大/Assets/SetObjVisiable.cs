﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetObjVisiable : MonoBehaviour {
    public GameObject obj;

    public void OnTriggerStay(Collider other) //穿透的物體有標籤[可閱讀物件] 獲取物件
    {
        if (other.tag == "Player")
        {
            obj.SetActive(true);
        }
    }
    public void OnTriggerExit(Collider other) //離開物件時  丟失物件
    {
        if (other.tag == "Player")
        {
            obj.SetActive(false);
        }
    }
}
