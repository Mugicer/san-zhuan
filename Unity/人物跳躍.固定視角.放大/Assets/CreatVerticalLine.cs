﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreatVerticalLine : MonoBehaviour {
    public GameObject VerticalLine;
    public GameObject[] Lines = new GameObject[1];
    public GameObject The_Next_Front;
    public int numbe_of_Line;
    public float distance;

    public float corner;
    // Use this for initialization
    public void SetDistance(float a) {
        distance = a;
    }
    public void SetNumber(int a)
    {
        numbe_of_Line = a;
        Lines = new GameObject[a];
        corner = 360 / a;
    }
    public void CreatLines() {
        for (int i = 0; i < numbe_of_Line; i++)
        {
            GameObject obj = Instantiate(VerticalLine,transform);
            Lines[i] = obj;
        }
	}
    public void ClearLines() {
        for (int i = 0; i < numbe_of_Line; i++)
        {
            Destroy(Lines[i]);
            Lines[i] = null;
        }
    }
	
	// Update is called once per frame
	void Update () {
        transform.rotation = Quaternion.Euler(0,0,0);
        if (Lines[0]!=null)
        {
            foreach (GameObject a in Lines)
            {
                if (a == null)
                {
                }
                else
                {
                    a.transform.position = transform.position + The_Next_Front.transform.forward * distance;
                    The_Next_Front.transform.Rotate(0, corner, 0);
                }

            }
        }

        
	}
}
