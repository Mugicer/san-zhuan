﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Light : MonoBehaviour {
    public GameObject player;
    public float range = 10;
    private void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player");    
    }
    void Update () {
        this.transform.position = player.transform.position + new Vector3(0, range, 0);
	}
   
}
