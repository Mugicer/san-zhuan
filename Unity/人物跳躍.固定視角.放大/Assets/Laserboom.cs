﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laserboom : MonoBehaviour {
    private LineRenderer LineRenderer;
    private CapsuleCollider capsule;
    public Material LaserDamage;
    private bool use = true;
    public int num_of_line;

    public float damage;
    public float wait_Time=0.5f;
    public float LineWidth;
    // Use this for initialization
    private void Awake()
    {
        LineRenderer =GetComponentInParent<LineRenderer>();
        capsule = GetComponent<CapsuleCollider>();
    }
    void Start () {
        LineWidth = LineRenderer.startWidth;
        capsule.radius = LineWidth/2;
        capsule.center = Vector3.zero;
        capsule.direction = 2;
        LineRenderer.material = LaserDamage;
    }

    // Update is called once per frame
    void Update() {

	}
    public void setLine(int i,Vector3 s,Vector3 e) {
        num_of_line = i;
        if (s==e)
        {
            transform.position = s ;
            transform.forward = s;
            capsule.height = 0;
        }
        else
        {
            transform.position = s + (e - s) / 2;
            transform.forward = s - e;
            capsule.height = (s - e).magnitude;
        }
    }
    public void OnTriggerStay(Collider other)
    {
        if (other.tag=="Player"&&use) {
            other.SendMessage("plusHP",damage);
            use = false;
            StartCoroutine(waitTime(wait_Time));
        }
    }
    IEnumerator waitTime(float t) {
        yield return new WaitForSeconds(t);
        use = true;
    }
}
