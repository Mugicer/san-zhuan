﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class OBJtalk : MonoBehaviour {
    public GameObject player;
    public player_input pi;
    public GameObject ObjInferrence;
    public GameObject targetOBJ;
    public bool self_off=true;
	// Use this for initialization
	void Start () {
        player = GameObject.FindWithTag("Player");
        ObjInferrence = this.transform.GetChild(0).gameObject;
        pi = player.GetComponent<player_input>();
    }
	
	// Update is called once per frame
	void Update () {
    }
    void Set_Meterial() {
        if (targetOBJ != null || targetOBJ.tag != "God")
        {
            ObjInferrence.transform.GetChild(0).GetChild(1).GetComponent<Text>().text = targetOBJ.GetComponent<Book>().text;//一長串是用來捕捉自身子物件呈現文字的地方

        }
    }
    void Set_Talk_On() {
        ObjInferrence.SetActive(true);
    }
    void Set_Talk_Off() {
        ObjInferrence.SetActive(false);
    }
}
