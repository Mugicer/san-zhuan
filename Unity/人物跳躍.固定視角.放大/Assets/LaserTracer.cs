﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserTracer : MonoBehaviour {
    public RaycastHit Hit;
    Vector3 start;
    Vector3 way;
    Vector3 point0;
    Vector3 point1;
    private LineRenderer lineRend;
    private CapsuleCollider capsule;
    public float LineWidth;
    public Transform target;
    public float waitsecond;
    public GameObject spot;
    private GameObject a;

    public bool gohead = true;
    public bool gotail = false;
    bool use = true;
    float trigger_Time = 0.3f;
    public float Laserspeed = 0.1F;
    private float speed;
    public int damage = 1;
    public float HitTimes;
    public float firstmove;
    float count;
    // Use this for initialization
    private void Awake()
    {
        lineRend = GetComponent<LineRenderer>();
        capsule = GetComponent<CapsuleCollider>();
        target = GameObject.FindWithTag("Player").transform;
    }
    void Start()
    {
        ResetLaser();
        lineRend.SetPosition(0, transform.position);
        lineRend.SetPosition(1, transform.position);
        Physics.Raycast(start, way, out Hit);
        //a = Instantiate(spot);
        //a.transform.position = start;
        //a = Instantiate(spot);
        //a.transform.position = start+way;
        //Debug.Log("00 start "+start+"    way "+way+"   hit.point "+Hit.point);
        a = Instantiate(spot);
        Destroy(spot, 20f);
        a.transform.position = Hit.point;
        //Debug.Log("01 start " + start + "    way " + way + "   hit.point " + Hit.point);
        point0 = lineRend.GetPosition(0);
        point1 = lineRend.GetPosition(1);
        firstmove = Hit.distance / Laserspeed;
        speed = Laserspeed;

        capsule.radius = LineWidth / 2;
        capsule.center = Vector3.zero;
        capsule.direction = 2;
    }
    void Update()
    {
        if (gohead)
        {
            head();
            if (lineRend.GetPosition(0) == Hit.point)
            {
                gohead = false;
                gotail = true;
                speed = Laserspeed;//重置速度
            }
        }
        if (gotail)
        {
            tail();
            if (lineRend.GetPosition(1) == Hit.point)
            {
                gotail = false;
                StartCoroutine(waittime());
                
                if (count == HitTimes)
                {
                    //Debug.Log(this.name);
                    Destroy(a);
                    Destroy(this.gameObject);
                }
            }
        }
        transform.position = point0 + (point1 - point0) / 2;
        transform.LookAt(point0);
        capsule.height = (point1 - point0).magnitude;

    }
        IEnumerator waittime() {
            start = Hit.point;//計算新向量
            transform.LookAt(target.position + new Vector3(0, 1, 0));
            way = transform.forward;
            Physics.Raycast(start, way, out Hit);
            a.transform.position = Hit.point;
            //Debug.Log(Hit.normal);
            firstmove = Hit.distance / Laserspeed;//計算新移動
            speed = Laserspeed;//重置速度
            count++;
            //Debug.Log(count);
            yield return new WaitForSeconds(waitsecond * 1f);
            gohead = true;
        }
    private void head()
    {
        point0 = Vector3.Lerp(point0, Hit.point, speed);
        Debug.Log(point0);
        speed = (firstmove / Vector3.Distance(point0, Hit.point)) * Time.deltaTime;
        lineRend.SetPosition(0, point0);
    }
    private void tail()
    {
        point1 = Vector3.Lerp(point1, Hit.point, speed);
        speed = (firstmove / Vector3.Distance(point1, Hit.point)) * Time.deltaTime;
        lineRend.SetPosition(1, point1);
    }
    private void ResetLaser()
    {
        start = transform.position;
        way = transform.forward;

    }
    public void OnTriggerStay(Collider other)
    {   
        if (other.tag == "Player" && use)
        {
            other.SendMessage("plusHP", damage);
            use = false;
            StartCoroutine(waitTime(trigger_Time));
        }

    }
    IEnumerator waitTime(float t)
    {
        yield return new WaitForSeconds(t);
        use = true;
    }
}
