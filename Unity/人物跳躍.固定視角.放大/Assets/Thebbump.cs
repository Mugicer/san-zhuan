﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Thebbump : MonoBehaviour {
    public float time;
    public float damage;
    public GameObject place;
    public GameObject bp;
    bool b = false;
	void Start () {
        Invoke("bump",time);
        
    }
	
    void bump() {
        
        b = true;
    }
    private void OnTriggerStay(Collider other)
    {
        if (b)
        {
            if (other.tag == "Player")
            {
                Debug.Log(other);
                other.SendMessage("plusHP", damage);
                b=false;
            }
            GameObject a = Instantiate(bp, transform);
            a.transform.parent = null;
            Destroy(this.gameObject);
        }
        
    }
}
