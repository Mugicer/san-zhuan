﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class player_output : MonoBehaviour {
    [Header("Managed GameObjects")]
    public BarControl playerHPMP;
    public BossBar bossHP;
    public player_input pi;
    public Animator anim;
    public GameObject model;
    public StoreData storedata;
    public GameObject gameUI;

    [Header("Player Settings")]
    public string Name = "Mugicer";
    [Range(0, 100)]
    public float xspeed = 60;
    [Range(0, 100)]
    public float yspeed = 30;
    [Range(0, 100)]
    public float CamaraLerp_move = 0.5f;
    [Range(0, 10)]
    public float range = 1;
    public float hop;//跳
    public GameObject Camara;
    public GameObject FreeObj;
    public GameObject Camara_Other;
    public GameObject ThirdPosition;//視角跟隨
    private Vector3 ThirdPositionset;
    public float ThirdDis;
    public GameObject bulletcreater;

    [Header("Objtalk settings")]
    public GameObject useTrigger;
    private Vector3 buset;
    private RaycastHit Hit;
    public GameObject Objtalk;
    public GameObject boss;
    public GameObject enemy;
    private Quaternion rotationEuler;
    public float distence = 1;
    public float shootparameter = 1;
    private bool onground = true;

    [Header("Player Table")]
    public float LifeOfTime ;

    public float movespeed ;//移速
    public int str ;
    public int defence ;
    public int MaxHitPoint ;
    public int HitPoint ;
    public int MaxManaPoint ;
    public int ManaPoint ;
    [Header("Bag table")]
    [Header("Dash Parameter")]
    public bool DashUsing=false;
    public int DashMp ;
    [Range(0, 100)]
    public float CamaraLerp_Dash = 0.5f;
    public bool DashCamara = false;
    public Vector3 DashWay;
    public float DashingTime;
    public float DashCamara_times;
    public float DashPower;
    private float x;
    private float y;
    private Rigidbody rigid;
    private Vector3 planarVec;
    public Vector3 thrustVec;//推力


    private void Awake()
    {
        storedata = GameObject.FindGameObjectWithTag("StoreData").GetComponent<StoreData>();
        Debug.Log("store");
        playerHPMP = GetComponent<BarControl>();
        bossHP = GetComponentInParent<BossBar>();
        pi = GetComponent<player_input>();
        rigid = GetComponent<Rigidbody>();
        anim = model.GetComponent<Animator>();
    }
    void Start () {

        LoadData();
        rotationEuler = this.transform.rotation;
        ThirdPositionset = ThirdPosition.transform.position - transform.position;
        buset = bulletcreater.transform.position - transform.position;
    }
	
	// Update is called once per frame
	void Update () {
        if (!pi.UIControling)
        {
            detectionmouse();
            gameUI.SetActive(true);
            jump();
        }else
        {
            gameUI.SetActive(false);
        }
        controlmove();
        playerupdate();//極少變更且持續更新
    }

    void FixedUpdate()
    {
        //rigid.velocity = pi.Dmag * pi.Dvec;
        rigid.velocity = new Vector3(planarVec.x  ,rigid.velocity.y, planarVec.z ) + thrustVec;
        thrustVec = Vector3.zero;
    }

    private void playerupdate()
    {
        nametext();//顯示名字
        controlfollow();
        controlturn();
        anim.SetFloat("Speed", pi.Dmag);//顯示速度
        anim.SetBool("Jump", pi.jump);
        anim.SetBool("Dash", DashUsing);
    }

    

    void detectionmouse()
    {
        ThirdDis = ThirdDis+ (-1) * 0.3f *Input.GetAxis("Mouse ScrollWheel");
        x = x + Input.GetAxis("Mouse X") * xspeed * Time.deltaTime;
        y = y - Input.GetAxis("Mouse Y") * yspeed * Time.deltaTime;
        if (ThirdDis > 1) { ThirdDis = 1; }
        if (ThirdDis <0.1) { ThirdDis = 0.1f; }
        if (x > 360) { x -= 360; }
        if (x < 0) { x += 360; }
        if (y < -20) { y = -20; }
        if (y > 90) { y = 90; }
    }
    void controlmove()
    {
        planarVec = pi.Dmag * pi.Dvec * movespeed * Time.deltaTime;//座標方面的移動
        if (pi.Dmag > 0.01f)
        {
            model.transform.forward = pi.Dvec * Time.deltaTime;//模型方向旋轉
        }
    }
        private void nametext()
    {
        gameObject.transform.GetChild(0).GetChild(0).GetComponent<Text>().text = Name;
        gameObject.transform.GetChild(0).forward = gameObject.transform.GetChild(0).position-Camara.transform.position;
    }

    public void Rename(string n)
    {
        Name = n;
    }
    void controlturn()
    {
        if (Camara_Other == null)
        {
            transform.rotation = Quaternion.Euler(0, x, 0);
        }
        else {
            transform.rotation = Quaternion.Euler(0, x, 0);
        }
    }
    void controlfollow()
    {
        
        rotationEuler = Quaternion.Euler(y, x, 0);//計算旋轉座標
        ThirdPosition.transform.rotation = rotationEuler;//刷新旋轉座標
        ThirdPosition.transform.position = transform.position + rotationEuler * ThirdPositionset*ThirdDis;
        if (Camara_Other == null)//有無指定特殊攝像機位置
        {
            
            Vector3 set = ThirdPosition.transform.position - transform.position;
            if (Physics.Raycast(transform.position + new Vector3(0, 1, 0), set, out Hit, Vector3.Magnitude(ThirdPositionset*ThirdDis)))//主體到預設攝像位置是否撞到物體
            {
                FreeObj.transform.position = Hit.point+Hit.normal * 0.3f;//有 停在物體前
                //Debug.Log("stuck");
                Camara.transform.position = Hit.point + Hit.normal*0.3f;
            }
            else {
                FreeObj.transform.position = ThirdPosition.transform.position;//沒 停在預設位置
            }

            //if (pi.Dmag < 0.1 && anim.GetFloat("Speed") == 0)//判定是否移動中
            //{//否
            //    Camara.transform.position = FreeObj.transform.position;
            //}
            if (DashCamara)
            {
                Camara.transform.position = Vector3.Lerp(Camara.transform.position, FreeObj.transform.position, CamaraLerp_Dash * Time.deltaTime);//Dash時
            }
            else {
                Camara.transform.position = Vector3.Lerp(Camara.transform.position, FreeObj.transform.position, CamaraLerp_move * Time.deltaTime);//移動時
            }

        }
        else {
            FreeObj.transform.position = Camara_Other.transform.position;//移至設定位置
        }

        if (Vector3.Magnitude(FreeObj.transform.position - Camara.transform.position) < range)//在距離內直接鎖定
        {
            Camara.transform.position = FreeObj.transform.position;
        }

        

        Camara.transform.LookAt(transform.position + new Vector3(0, 1, 0));//看向目標點

        //Camara.transform.LookAt(transform.position+new Vector3(0,1.5f,0));
        //if (pi.Dmag<0.1)
        //{
        //    Camara.transform.position= FreeObj.transform.position;
        //    Camara.transform.LookAt(transform.position+ new Vector3(0, 1, 0));
        //}

        if (boss != null|| enemy != null)
        {
            if (enemy!=null)
            {
                bulletcreater.transform.LookAt(new Vector3(enemy.transform.position.x, bulletcreater.transform.position.y, enemy.transform.position.z));
            }
            else
            {
                bulletcreater.transform.LookAt(new Vector3(boss.transform.position.x,bulletcreater.transform.position.y,boss.transform.position.z));
            }
            
        }
        else
        {
            //Debug.Log(x+","+y);
            bulletcreater.transform.rotation = Quaternion.Euler(0, x, 0);
        }
            bulletcreater.transform.position = transform.position + Quaternion.Euler(0, x, 0) * buset;
        
        model.transform.position = transform.position;
    }
    void jump()
    {
        if (pi.jump)
        {
            if (onground)
            {
                thrustVec = new Vector3(0, hop, 0);
                StartCoroutine(down());
                onground = false;
            }
        }
    }
        IEnumerator down()
        {
            yield return new WaitForSeconds(0.3f);
            thrustVec = new Vector3(0, -hop*1 / 1, 0);
        }

    public void OnCollisionEnter(Collision col)
    {
        onground = true;
    }

    public void SaveData() {
        Debug.Log("save01");
        StoreData.Instance.Name               = Name                       ;
        StoreData.Instance.xspeed             = xspeed                     ;
        StoreData.Instance.yspeed             = yspeed                     ;
        StoreData.Instance.CamaraLerp_move    = CamaraLerp_move            ;
        StoreData.Instance.range              = range                      ;
        StoreData.Instance.hop                = hop                        ;
        StoreData.Instance.distence           = distence                   ;
        StoreData.Instance.shootparameter     = shootparameter             ;
        StoreData.Instance.LifeOfTime         = LifeOfTime                 ;
        StoreData.Instance.movespeed          = movespeed                  ;
        StoreData.Instance.str                = str                        ;
        StoreData.Instance.defence            = defence                    ;
        StoreData.Instance.MaxHitPoint        = MaxHitPoint                ;
        StoreData.Instance.HitPoint           = HitPoint                   ;
        StoreData.Instance.MaxManaPoint       = MaxManaPoint               ;
        StoreData.Instance.ManaPoint          = ManaPoint                  ;
        StoreData.Instance.DashMp             = DashMp                     ;
        StoreData.Instance.CamaraLerp_Dash    = CamaraLerp_Dash            ;
        StoreData.Instance.DashingTime        = DashingTime                ;
        StoreData.Instance.DashCamara_times   = DashCamara_times           ;
        StoreData.Instance.DashPower          = DashPower                  ;
        
    }
    public void LoadData() {
        Debug.Log("Load");
         Name             = StoreData.Instance.Name                         ;
         xspeed           = StoreData.Instance.xspeed                       ;
         yspeed           = StoreData.Instance.yspeed                       ;
         CamaraLerp_move  = StoreData.Instance.CamaraLerp_move              ;
         range            = StoreData.Instance.range                        ;
         hop              = StoreData.Instance.hop                          ;
         distence         = StoreData.Instance.distence                     ;
         shootparameter   = StoreData.Instance.shootparameter               ;
         LifeOfTime       = StoreData.Instance.LifeOfTime                   ;
         movespeed        = StoreData.Instance.movespeed                    ;
         str              = StoreData.Instance.str                          ;
         defence          = StoreData.Instance.defence                      ;
         MaxHitPoint      = StoreData.Instance.MaxHitPoint                  ;
         HitPoint         = StoreData.Instance.HitPoint                     ;
         MaxManaPoint     = StoreData.Instance.MaxManaPoint                 ;
         ManaPoint        = StoreData.Instance.ManaPoint                    ;
         DashMp           = StoreData.Instance.DashMp                       ;
         CamaraLerp_Dash  = StoreData.Instance.CamaraLerp_Dash              ;
         DashingTime      = StoreData.Instance.DashingTime                  ;
         DashCamara_times = StoreData.Instance.DashCamara_times             ;
         DashPower        = StoreData.Instance.DashPower                    ;
        
    }

}
