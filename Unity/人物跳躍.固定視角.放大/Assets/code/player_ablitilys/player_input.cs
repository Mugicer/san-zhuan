﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class player_input : MonoBehaviour {

    [Header("key settings")]
    public string keyUp = "w";
    public string keyDown = "s";
    public string keyLeft = "a";
    public string keyRight = "d";
    public string keyEsc = "escape";
    public string keyPause = "p";
    public string keyJump = "space";
    public string keyDash = "left shift";
    public string keyA = "1";
    public string keyTalk = "f";
    public string keyC = "3";
    public string keyD = "4";

    [Space(10)]
    [Header("output singals")]
    public bool autocatch = true;
    public bool UIControling = false;
    private bool onground = true;
    public float Dup;
    public float Dright;
    public float Dmag;  // magnitude
    [Range(0, 1)]
    public float Dmag_Lerp = 0.5f;
    public Vector3 Dvec;  // vector
    [Range(0, 1)]
    public float Dvec_Lerp = 0.5f;
    public bool run;
    public bool jump;
    public bool dash;
    public bool talk;

    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        controlmove();

    }

    void controlmove()
    {
        Dup = (Input.GetKey(keyUp) ? 1 : 0) - (Input.GetKey(keyDown) ? 1 : 0);
        Dright = (Input.GetKey(keyRight) ? 1 : 0) - (Input.GetKey(keyLeft) ? 1 : 0);

        float tempDmag;
        tempDmag = Mathf.Sqrt((Dup * Dup + Dright * Dright));

        Dmag = Mathf.Lerp(Dmag, tempDmag, Dmag_Lerp);
        if (Dmag>1)
        {
            Dmag = 1;
        }

        Vector3 tempDvec;
        tempDvec = (Dup * transform.forward + Dright * transform.right).normalized;
        tempDvec.y = 0;
        Dvec = Vector3.Slerp(Dvec, tempDvec, Dvec_Lerp);

        run = Input.GetKey(keyA);
        jump = Input.GetKey(keyJump);
        dash = Input.GetKey(keyDash);
        talk = Input.GetKeyDown(keyTalk);
    }
}
