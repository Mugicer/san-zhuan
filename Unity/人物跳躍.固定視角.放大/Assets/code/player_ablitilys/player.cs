﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class player : MonoBehaviour {
        public bool UIControling = false;
        public  string Name = "Mugicer";
        // Use this for initialization
    private Rigidbody rb;
        public float positionspeed;//移速
        public float hop;//跳
    public GameObject backcamara;//視角跟隨
    private Vector3 bcset;
    public GameObject eyecamara;
    private Vector3 ecset;
    public GameObject bulletcreater;
    private Vector3 buset;
    public GameObject boss;
        public bool autocatch=true;
        public float LifeOfTime = 60;
        //public GameObject groundcheck;
        //private Vector3 gcset;
    private float x;
    private float y;
        public float xspeed = 60;
        public float yspeed = 30;
    public Quaternion rotationEuler;
        public float distence = 1;
        public float shootparameter = 1;
        private bool onground = true;
    void Start () {
        rb = GetComponent<Rigidbody>();
        bcset = backcamara.transform.position - transform.position;
        backcamara.SetActive(true);
        ecset = eyecamara.transform.position - transform.position;
        eyecamara.SetActive(false);
        buset = bulletcreater.transform.position - transform.position;
    }
	
	// Update is called once per frame
	void Update () {
        if (!UIControling) { 
            detectionmouse();
            controlmove();
            jump();
        }
        nametext();
        controlfollow();
        controlturn();

        if (LifeOfTime > 0)
        {
            LifeOfTime -= 1 * Time.deltaTime;
        }
    }

    void detectionmouse() {
        x = x + Input.GetAxis("Mouse X") * xspeed * Time.deltaTime;
        y = y - Input.GetAxis("Mouse Y") * yspeed * Time.deltaTime;
        if (x > 360) { x -= 360; }
        if (x < 0) { x += 360; }
        if (y < -20) { y = -20; }
        if (y > 90) { y = 90; }
    }
    void controlmove()
    {
        if (Input.GetKey(KeyCode.W))
        {
            transform.Translate(0, 0, 2 * positionspeed * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.S))
        {
            transform.Translate(0, 0, -2 * positionspeed * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.A))
        {
            transform.Translate(-2 * positionspeed * Time.deltaTime, 0, 0);
        }
        if (Input.GetKey(KeyCode.D))
        {
            transform.Translate(2 * positionspeed * Time.deltaTime, 0, 0);
        }

    }
    private void nametext()
    {
        gameObject.transform.GetChild(0).GetChild(0).GetComponent<Text>().text = Name;
        gameObject.transform.GetChild(0).rotation = backcamara.transform.rotation;
    }

    public void Rename(string n)
    {
        Name = n;
    }
    void controlturn() {
        transform.rotation = Quaternion.Euler(0, x, 0);
    }
    void controlfollow() {
        if (Input.GetMouseButton(1) == true)
        {
            backcamara.SetActive(false);
            eyecamara.SetActive(true);
            autocatch = false;
        }
        else {
            backcamara.SetActive(true);
            eyecamara.SetActive(false);
            autocatch = true;
        }
        rotationEuler = Quaternion.Euler(y, x, 0);//計算旋轉座標
        backcamara.transform.rotation = rotationEuler;//刷新旋轉座標
        eyecamara.transform.rotation = rotationEuler;
        if (autocatch&&boss!=null)
        {
            bulletcreater.transform.LookAt(boss.transform.position);
        }
        else
        {
            bulletcreater.transform.rotation = rotationEuler;
        }
        eyecamara.transform.position = transform.position + rotationEuler * ecset;
        backcamara.transform.position = transform.position + rotationEuler * bcset;
        bulletcreater.transform.position = transform.position + rotationEuler * buset;
        //groundcheck.transform.position = transform.position + gcset;
    }
    void jump() {
        if (Input.GetKeyDown(KeyCode.Space)) {
            if (onground)
            {
                rb.AddForce(0, hop, 0);
                StartCoroutine(down());
                onground = false;
            }
        }
    }
        IEnumerator down() {
            yield return new WaitForSeconds(0.3f);
            rb.AddForce(0, -hop/2, 0);
        }
    public void OnCollisionEnter(Collision col)
    {
        onground = true;
    }
    /*void setonground() {
        Debug.Log("onground");
        onground = true;
    }*/
}
