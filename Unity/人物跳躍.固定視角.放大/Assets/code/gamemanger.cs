﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class gamemanger : MonoBehaviour {

    private bool anyUIopen = false;
    private bool pause = false;
    public GameObject player;
    private GameObject x;
    public TheGod God;
    public GameObject Boss;
    public GameObject Boss_Blood;
    public GameObject bulletcreater;
    public GameObject Objtalk;
    public GameObject endtext;
    private void Start()
    {
        x= GameObject.FindGameObjectWithTag("God");
        if (x!=null)
        {
            God = x.GetComponent<TheGod>();
        }

    }
    void Update()
        {
            player.GetComponent<player_input>().UIControling = anyUIopen;
            Boss = GameObject.FindGameObjectWithTag("boss");
            CheckUI();
            CheckBtn();
            CheckBoss();
            SetMouseActive();
            Checkend();
        }

        private void CheckBoss()
        {
            if (Boss == null)
            {
                Boss_Blood.SetActive(false);
            }
            else {
                Boss_Blood.SetActive(true);
            }
        }

    private void CheckUI()
    {
        if (God != null)
        {
            if (God.GetComponent<Animator>().GetBool("inUI")|| Objtalk.transform.GetChild(0).gameObject.active)
            {
                anyUIopen = true;
            }
            else
            {
                anyUIopen = false;
            }
        }
        else
        { 
            if (Objtalk.transform.GetChild(0).gameObject.active)
            {
                anyUIopen = true;
            }
            else
            {
                anyUIopen = false;
            }
        }
    }
        private void CheckBtn()
        {
            if (Input.GetKeyDown("p"))
            {
                if (Time.timeScale == 0)
                {
                    Time.timeScale = 1;
                }
                else {
                    Time.timeScale = 0;
                }

            }
            if (Input.GetKeyDown("o"))
            {
                Time.timeScale = 1;
                print(Time.timeScale);
                SceneManager.LoadScene("魁儡師");
                print(Time.timeScale);
            }
            if (Input.GetKeyDown("escape")) {
                SceneManager.LoadScene("進入畫面");
            }
            if (Input.GetKeyDown("q")) {
                bulletcreater.GetComponent<bulletcreater1>().now++;
            }
            if (Input.GetKeyDown("e"))
            {
                bulletcreater.GetComponent<bulletcreater1>().now--;
            }
        }


        private void SetMouseActive()
        {
            if (anyUIopen)
            {
                Cursor.visible = true;
            }
            else
            {
                Cursor.visible = false;
            }
        }
        public void Checkend()
        {
            if (player.GetComponent<player_output>().HitPoint < 1 && !pause) {
                //Time.timeScale = 0;
                //endtext.transform.GetComponent<Text>().text = "你死了";
                //endtext.SetActive(true);
                //pause = true;
            }
            //else if (transform.GetComponent<BossBar>().hpbarscript.now < 1 && !pause)
            //{
            //    //Time.timeScale = 0;
            //    //endtext.transform.GetComponent<Text>().text = "你贏了";
            //    //endtext.SetActive(true);
            //    //pause = true;
            //    if (Boss != null)
            //    {
            //        Boss.SendMessage("Dead");
            //    }
            //}
            if (player.GetComponent<player_output>().LifeOfTime < 0 && !pause) {
                Time.timeScale = 0;
                endtext.transform.GetComponent<Text>().text = "時間耗盡了";
                endtext.SetActive(true);
                pause = true;
            }
        }

    
}
