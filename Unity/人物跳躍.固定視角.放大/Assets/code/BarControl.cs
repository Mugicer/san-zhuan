﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarControl : MonoBehaviour {
    public GameObject HPbar;
    public GameObject MPbar;
    private Bar hpbarscript;
    private Bar mpbarscript;
    public int mprecover=1;
    public float mprecover_CD;
    public bool mprecover_true=true;

    private player_output po;

    void Awake () {
        hpbarscript = HPbar.GetComponent<Bar>();
        mpbarscript = MPbar.GetComponent<Bar>();
        po = GetComponent<player_output>();
    }
    private void Start()
    {
        hpbarscript.Max = po.MaxHitPoint;
        mpbarscript.Max = po.MaxManaPoint;
    }

    void Update () {
        hpbarscript.now = po.HitPoint;
        mpbarscript.now = po.ManaPoint;
        if (mprecover_true)
        {
            plusMP(mprecover);
            mprecover_true = false;
            StartCoroutine(mprecover_count(mprecover_CD));
        }

    }
        IEnumerator mprecover_count(float mprecover_CD)
        {
            yield return new WaitForSeconds(mprecover_CD);
            mprecover_true = true;
        }

    public void plusHP(float HP)
    {
        plusHP((int)HP);
    }
    public void plusHP(int a)
    {
        float b = a + po.HitPoint;
        if (b> po.MaxHitPoint)
        {
            po.HitPoint = po.MaxHitPoint;
        }else if (b<1) {
            po.HitPoint = 0;
        }else{
            po.HitPoint = (int)b;
        }
    }
    public void plusMP(int a)
    {
        float b = a + po.ManaPoint;
        if (b > po.MaxManaPoint)
        {
            po.ManaPoint = po.MaxManaPoint;
        }
        else if (b < 1)
        {
            po.ManaPoint = 0;
        }
        else
        {
            po.ManaPoint = (int)b;
        }
    }

    public void changeMaxHP(int a)
    {
        po.MaxHitPoint = a;
        po.HitPoint = a;
    }
    public void changeMaxMP(int a)
    {
        po.MaxHitPoint = a;
        po.ManaPoint = a;
    }
}
