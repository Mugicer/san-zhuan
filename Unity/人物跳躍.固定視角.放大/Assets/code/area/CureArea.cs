﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CureArea : MonoBehaviour {

    // Use this for initialization
    private Renderer rend;
    public int heal = 100;
    public float waitsec = 0.1f;
    private bool use=true;
	void Start () {
        rend = GetComponent<Renderer>();
        rend.enabled = false;
    }
	
	// Update is called once per frame
	void Update () {

    }
    public void OnTriggerStay(Collider other)
    {
        CheckPlayer(other);
    }
    private void CheckPlayer(Collider other) {
        if (other.tag == "Player"&& use) {
            //Debug.Log("Player in area.");
            other.SendMessage("plusHP",heal);
            use = false;
            StartCoroutine(colddown());
        }
    }
    private IEnumerator colddown()
    {
        yield return new WaitForSeconds(waitsec);
        use = true;
    }
}
