﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class catchenemy : MonoBehaviour {
    public GameObject boss;
    public GameObject enemy;
    public player_output po;
	// Use this for initialization
	void Start () {
        po = GetComponentInParent<player_output>();
	}
	
	// Update is called once per frame
	void Update () {
        po.boss = boss;
        po.enemy = enemy;
	}
    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "boss")
        {
            boss = other.gameObject;
        }
        if (other.tag == "enemy")
        {
            enemy = other.gameObject;
        }

    }
    public void OnTriggerStay(Collider other)
    {

    }
    public void OnTriggerExit(Collider other)
    {
        if (other.tag == "boss")
        {
            boss = null;
        }
        if (other.tag == "enemy")
        {
            enemy = null;
        }

    }
}
