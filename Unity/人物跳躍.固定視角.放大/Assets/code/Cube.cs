﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Cube : MonoBehaviour {
    Renderer R1;
    public GameObject self;
	void Start () {
        R1 = gameObject.GetComponent<Renderer>();
	}
	
	void Update () {
		
	}
    void Hitbyplayer() {
        if (Input.GetButtonDown("E")) {
            if (R1.material.color == Color.yellow) 
            {
                R1.material.color = Color.green; 
            }
            else
            {
                R1.material.color = Color.yellow; 
            }
        }
    }
    void Hit() {
        Invoke("invisable", 1);
        Invoke("visable",3);
    }
    void invisable()
    {
        self.SetActive(false);
    }
    void visable() {
        self.SetActive(true);
    }
}
