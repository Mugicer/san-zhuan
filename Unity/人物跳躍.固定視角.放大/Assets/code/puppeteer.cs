﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class puppeteer : MonoBehaviour {
    public Animator anim;
    public GameObject player;
    public GameObject puppet;
    public GameObject position;
    public GameObject[] petarr;
    public GameObject[] high;
    public GameObject treture;
    
    public GameObject HPbar;
    public Vector3 offset =new Vector3(0,0,5);
    public Quaternion ro;
        private float Y=90f;
    public int num = 5;
    public int numcount = 0;
    public bool Detect=false;
    public int HP = 20;
    public int MAXHP = 999;


	void Start ()
    {
        anim = GetComponent<Animator>();
        petarr=new GameObject[num];
        ro = Quaternion.Euler(0, Y, 0);
        player = GameObject.FindGameObjectWithTag("Player");
        defaltset();
        HPbar.GetComponent<Bar>().Max = MAXHP;
        HPbar.GetComponent<Bar>().now = HP;
    }
    public void start()
    {
        anim.SetBool("Action", true);
    }

    private void defaltset()//創造傀儡 創造位置
        {
            for (int i = 0; i < num; i++)
            {
                GameObject pet=Instantiate(puppet, transform.position + ro*offset, ro);
                GameObject po=Instantiate(position, transform.position + ro * offset, ro, transform);
                petarr[i] = pet;
                pet.GetComponent<puppet>().num = i;
                pet.GetComponent<puppet>().player = player;
                pet.GetComponent<puppet>().po = po;
                pet.GetComponent<puppet>().puppeteer = this;
                Y = Y + 360 / num;
                ro = Quaternion.Euler(0, Y, 0);
            }
        }
    
    void Update(){
        if (ro.y > 360) { ro.y = ro.y - 360; }
        anim.SetFloat("PlayerRange", Vector3.Distance(transform.position, player.transform.position));//計算與玩家的距離
        HPbar.GetComponent<Bar>().Max = MAXHP;
        HPbar.GetComponent<Bar>().now = HP;
    }
    public void plusHP(float HP)
    {
        plusHP((int)HP);
    }
    public void plusHP(int a)
    {
        float b = a + HP;
        if (b > MAXHP)
        {
            HP = MAXHP;
        }
        else if (b < 1)
        {
            Dead();
        }
        else
        {
            HP = (int)b;
        }
    }
    public void Dead()
    {
        if (treture != null)
        {
            treture.SetActive(true);
        }
        foreach (GameObject a in petarr)
        {
            Destroy(a);
        }
        StoreData.Instance.puppet = true;
        Destroy(this.gameObject);
    }



}
