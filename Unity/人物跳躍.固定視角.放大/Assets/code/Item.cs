﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu]
public class Item : MonoBehaviour
{

    public Sprite sprite;
}
