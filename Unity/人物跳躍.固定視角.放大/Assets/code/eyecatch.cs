﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class eyecatch : MonoBehaviour {
    Ray ray;
    float raylength = 3f;
    RaycastHit hit;
	// Use this for initialization
	void Start () {
        ray = Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 0));
    }
	
	// Update is called once per frame
	void Update () {
        ray = Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 0));
        if (Physics.Raycast(ray, out hit, raylength)) {
            hit.transform.SendMessage("Hitbyplayer", gameObject, SendMessageOptions.DontRequireReceiver);
            print(hit.transform.name);
        }
        else{
        }
	}
}
