﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class bullet : MonoBehaviour {
    public GameObject real;
    public GameObject self;
    public Sprite image;
    public int Damage = 2;
    public float speed = 5;
    private bool inhand = true;
    public int paremeter = 1;
    private float p;
    public Vector3 scale;
    private Quaternion rota ;
    // Use this for initialization
    void Start () {
        p = Time.deltaTime*speed;
        scale = new Vector3(Time.deltaTime* 1, Time.deltaTime* 1, Time.deltaTime* 1);
        //放大的參數
    }

    // Update is called once per frame
    void Update() {
        if (inhand)
        {
            if (Input.GetMouseButton(0))
            {
                Debug.Log("M check");
                real.transform.localScale += scale;
            }
            if (Input.GetMouseButtonUp(0))
            {
                inhand = false;
                transform.parent = null;
            }
        }
        else {
            transform.Translate(0, 0, p*Time.timeScale);
            Destroy(self, 10);

        }
    }
}
