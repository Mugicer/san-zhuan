﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
[CreateAssetMenu]
public class NPC : MonoBehaviour {

    public  string Name = "yayu";
    public Transform target;
    public GameObject GM;
    public string talk = "";
    public string chooseA;
    public string chooseB; 

    public void Update()
    {
        nametext();
    }
    void Hitbyplayer()
    {
        if (Input.GetButtonDown("E"))
        {
            GM.SendMessage("talk");
        }
    }
    private void nametext()
    {
        gameObject.transform.GetChild(0).GetChild(0).GetComponent<Text>().text = Name;
        gameObject.transform.GetChild(0).rotation = target.rotation;
    }

    public void Rename(string n) {
        Name = n;
    }
}
