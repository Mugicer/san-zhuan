﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemybullet : MonoBehaviour {
    public GameObject real;
    public float speed = 5;
    public float destorytime = 3;
    private float p;
    void Start()
    {
        p = Time.deltaTime * speed;
        Destroy(gameObject, destorytime);
        //放大的參數
    }

    // Update is called once per frame
    void Update () {
        
        transform.Translate(0, 0, p);
    }

}
