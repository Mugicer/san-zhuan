﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Bar : MonoBehaviour {
    public int Max = 999;
    public int now = 800;
    public GameObject textArea;
    private Slider slider;
    private Text text;
	// Use this for initialization
	void Start () {
        textArea = gameObject.transform.GetChild(2).gameObject;
        text = textArea.GetComponent<Text>();
        slider = this.GetComponent<Slider>();
	}
	
	// Update is called once per frame
	void Update () {
        slider.maxValue = Max;
        slider.value = now;
        text.text = now+" / "+Max;
	}
}
