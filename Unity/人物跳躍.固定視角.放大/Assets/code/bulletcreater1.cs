﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class bulletcreater1 : MonoBehaviour {
    public GameObject[] bullet;
    public player_input pi;
    public bulletbox bulletbox;
    public int num=2;
    public int now = 0;
    private GameObject copy;
    // Use this for initialization
    void Start()
    {

        pi = GameObject.FindGameObjectWithTag("Player").GetComponent<player_input>();

    }

    // Update is called once per frame
    void Update()
    {
        if (now < 0)
        {
            now = num - 1;
        }
        if (now>num-1) {
            now = 0;
        }
        if (Input.GetMouseButtonDown(0)&& !pi.UIControling)
        {
            copy = Instantiate(bullet[now], transform.position, transform.rotation);
            copy.transform.parent = transform;
        }

        bulletbox.images[1] = bullet[now].GetComponent<bullet>().image;//中

        if (now==num-1 || bullet[now+1]==null)//右
        {
            bulletbox.images[2] = bullet[0].GetComponent<bullet>().image;
        }
        else
        {
            bulletbox.images[2] = bullet[now+1].GetComponent<bullet>().image;
        }
        if (now == 0)//左
        {
            int x=1;
            for (int i = num-1; i >=0; i--)
            {
                if (bullet[i]!=null)
                {
                    x = i;
                    break;
                }
            }
            bulletbox.images[0] = bullet[x].GetComponent<bullet>().image;
        }
        else
        {
            bulletbox.images[0] = bullet[now -1].GetComponent<bullet>().image;
        }
    }
    public void AddBullet(GameObject a) {
        for (int i = 0; i < num; i++)
        {
            if (bullet[i]==null)
            {
                bullet[i] = a;
                break;
            }
        }

    }
}
