﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class puppet : MonoBehaviour {
    private LineRenderer line;
    public GameObject player;
    public puppeteer puppeteer;
    public Animator anim;
    public GameObject po;
    public GameObject attackplace;
    private GameObject a;
    public int num;


    public float fixdis = 0.1f;
    public float speed =20 ;
    public Vector3 target;
        private Vector3 targetposition;

    void Start() {
        line = gameObject.GetComponent<LineRenderer>();
        anim = GetComponent<Animator>();
    }
    
    void Update() {
        updating();
    }
    
    private void updating()
    {
        line.SetPosition(0, transform.position);
        line.SetPosition(1, puppeteer.transform.position);//魁儡線*1
        if (target!=new Vector3(0,0,0))
        {
            targetposition = target;
        }
        
    }
    
    public void idle()
    {
        target = po.transform.position;
    }
    public void attack() {
        target = player.transform.position;
    }

    public void creatattack() {
        a=Instantiate(attackplace,transform);
    }
    public void deleteattack() {
        Destroy(a);
    }
    public void nulltarget()
    {
        target = new Vector3(0,0,0);
    }
    public void Look() {
        transform.LookAt(new Vector3(targetposition.x,transform.position.y,targetposition.z));
    }
    public void TP(Vector3 a)
    {
        transform.position = a;
    }
    public bool Move()
    {
        transform.position = Vector3.MoveTowards(transform.position, targetposition, speed * Time.deltaTime);//移動到目標
        if (transform.position == targetposition)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public bool Protect() {
        transform.position = Vector3.MoveTowards(transform.position, po.transform.position, speed * Time.deltaTime);
        if (Vector3.Distance(transform.position, po.transform.position) < fixdis)
        {
            return true;
        }
        else
        {
            return false;
        }
    }


}
