﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossBar : MonoBehaviour {
    public GameObject HPbar;
    public Bar hpbarscript;
    public int hp;
    // Use this for initialization
    void Awake()
    {
        hpbarscript = HPbar.GetComponent<Bar>();
        hp = HPbar.GetComponent<Bar>().now;
    }
    void Update()
    {

    }
    public void plusHP(int a)
    {
        Debug.Log(hpbarscript);
        float b = a;
        b = a + hpbarscript.now;
        if (b > hpbarscript.Max)
        {
            hpbarscript.now = hpbarscript.Max;
        }
        else if (b < 1)
        {
            hpbarscript.now = 0;
        }
        else
        {
            hpbarscript.now = (int)b;
        }
    }
    public void changeMaxHP(int a)
    {
        if (hpbarscript.now > a)
        {
            hpbarscript.now = a;
        }
        hpbarscript.Max = a;

    }
}
