﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LifeofTime : MonoBehaviour {
    public player_output player;
    public GameObject[] test = new GameObject[10];
    public Text text;
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<player_output>();
        test = GameObject.FindGameObjectsWithTag("Player");
        text = GetComponentInChildren<Text>();
    }
	// Update is called once per frame
	void Update () {
        text.text = ((int)player.LifeOfTime).ToString();

    }
}
