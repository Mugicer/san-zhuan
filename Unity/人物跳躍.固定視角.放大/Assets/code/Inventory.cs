﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Inventory : MonoBehaviour {
    public Image[] itemImages = new Image[numofitem];
    public Text[] counts = new Text[numofitem];
    public Item[] items = new Item[numofitem];
    public const int numofitem = 20;
    int itemCount = 0;
    public void Start()
    {
        for (int i = 0; i < items.Length; i++)
        {
            itemImages[i] = this.transform.GetChild(i).transform.GetChild(1).GetComponent<Image>();
            counts[i] = this.transform.GetChild(i).transform.GetChild(1).transform.GetChild(0).GetComponent<Text>();
        }
    }
    public void AddItem(Item itemToAdd)
    {
        for (int i = 0; i < items.Length; i++)
        {
            if (items[i] == itemToAdd)
            {
                itemCount += 1;
                counts[i].text = itemCount.ToString();
                break;
            }
            else if (items[i] == null)
            {
                items[i] = itemToAdd;
                itemImages[i].sprite = itemToAdd.sprite;
                itemImages[i].enabled = true;
                return;
            }
        }
    }
    public void RemoveItem(Item itemToRemove)
    {
        for (int i = 0; i < items.Length; i++)
        {
            if (items[i] == itemToRemove && itemCount > 1)
            {
                if (itemCount == 2)
                {
                    itemCount -= 1;
                    counts[i].text = "";
                }
                else
                {
                    itemCount -= 1;
                    counts[i].text = itemCount.ToString();
                }
            }
            else if (items[i] == itemToRemove)
            {
                items[i] = null;
                itemImages[i].sprite = null;
                itemImages[i].enabled = false;
                return;
            }
        }
    }
}
