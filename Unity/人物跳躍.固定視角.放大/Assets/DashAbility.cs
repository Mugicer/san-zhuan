﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DashAbility : MonoBehaviour {
    [Header("Managed GameObjects")]
    public player_input pi;
    public player_output po;
    
    // Use this for initialization
    private void Awake()
    {
        pi = GetComponent<player_input>();
        po = GetComponent<player_output>();
    }
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (!pi.UIControling)
        {
            dash();
        }
	}
    void dash()
    {
        if (pi.dash)
        {
            if (po.ManaPoint - po.DashMp > 0 && !po.DashUsing)
            {
                po.ManaPoint -= po.DashMp;
                po.DashUsing = true;
                po.DashCamara = true;
                po.DashWay = pi.Dvec;
                po.thrustVec += po.DashWay * po.DashPower;
                StartCoroutine(inDash());
            }
        }
    }
    IEnumerator inDash()
    {
        yield return new WaitForSeconds(po.DashCamara_times);
        po.DashCamara = false;
        yield return new WaitForSeconds(po.DashingTime - po.DashCamara_times);
        po.DashUsing = false;
    }
}
