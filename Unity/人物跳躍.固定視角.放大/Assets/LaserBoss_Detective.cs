﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserBoss_Detective : StateMachineBehaviour {
    LaserBoss self;
    Transform player;
    int r = 0;
	 // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        self = animator.transform.GetComponent<LaserBoss>();
        player = self.player.transform;
        r = Random.Range(0, 2);

    }

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        self.transform.LookAt(new Vector3(player.position.x , self.transform.position.y, player.position.z ));
        float dis = Vector3.Distance(self.player.transform.position ,  self.transform.position);
        if (dis <= animator.GetFloat("MaxDistance") / 4 * 3)
        {
            animator.SetBool("LaserAround", true);
            if (r==0)
            {

                animator.SetBool("EyeLine", false);
            }
            else
            {
                animator.SetBool("LaserTrace", false);
            }
            
        }
        else {
            animator.SetBool("LaserAround", false);
            if (r == 0)
            {

                animator.SetBool("EyeLine", true);
            }
            else
            {
                animator.SetBool("LaserTrace", true);
            }
        }
        animator.SetBool("LaserTrace", true);

    }

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	    
	}

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
