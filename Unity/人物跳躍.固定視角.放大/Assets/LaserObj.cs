﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserObj : MonoBehaviour {
    public RaycastHit Hit;
     Vector3 start;
     Vector3 way;
     Vector3 point0;
     Vector3 point1;
    private LineRenderer lineRend;
    public CapsuleCollider capsule;
    public BarControl barControl;
    public GameObject gameManeger;
    public float LineWidth;
    private LayerMask Mask;
    public float waitsecond;

    public bool gohead = false;
    public bool gotail = false;
    public float Laserspeed = 0.1F;
    private float speed;
    public int damage=1;
    public float HitTimes;
    public float firstmove;
    float count;
    // Use this for initialization
    private void Awake()
    {
        lineRend = GetComponent<LineRenderer>();
        capsule = GetComponent<CapsuleCollider>();
        gameManeger = GameObject.FindWithTag("gamemaneger");
        barControl = gameManeger.GetComponent<BarControl>();
    } 
    void Start () {
        ResetLaser();
        lineRend.SetPosition(0, transform.position);
        lineRend.SetPosition(1, transform.position);
        Physics.Raycast(start, way, out Hit);
        point0=lineRend.GetPosition(0);
        point1=lineRend.GetPosition(1);
        firstmove = Hit.distance / Laserspeed;
        speed = Laserspeed;
        Mask = 1 << 9+1<<2;

        capsule.radius = LineWidth / 2;
        capsule.center = Vector3.zero;
        capsule.direction = 2;
    }
	void Update () {
        if (gohead) {
            head();
            if (lineRend.GetPosition(0) == Hit.point)
            {
                gohead = false;
                gotail = true;
                speed = Laserspeed;//重置速度
            }
        }
        if (gotail)
        {
            tail();
            if (lineRend.GetPosition(1)==Hit.point)
            {
                gotail = false;
                StartCoroutine(waittime());
                start = Hit.point;//計算新向量
                way = Hit.normal * 2 + way;
                Physics.Raycast(start, way, out Hit,Mask);
                //Debug.Log(Hit.collider);
                //Debug.Log(Hit.normal);
                firstmove = Hit.distance / Laserspeed;//計算新移動
                speed = Laserspeed;//重置速度
                count++;
                //Debug.Log(count);
                if (count==HitTimes)
                {
                    //Debug.Log(this.name);
                    Destroy(this.gameObject);
                }
            }
        }
        capsule.transform.position = point0 + (point1 - point0) / 2;
        capsule.transform.LookAt(point0);
        capsule.height = (point1 - point0).magnitude;

    }
        IEnumerator waittime()
        {
            yield return new WaitForSeconds(waitsecond);
            gohead = true;
        }
    private void head()
    {
        point0 = Vector3.Lerp(point0, Hit.point, speed);
        //Debug.Log(point0);
        speed = (firstmove / Vector3.Distance(point0, Hit.point))*Time.deltaTime;
        lineRend.SetPosition(0, point0);
    }
    private void tail() {
        point1 = Vector3.Lerp(point1, Hit.point, speed);
        speed = (firstmove / Vector3.Distance(point1, Hit.point)) * Time.deltaTime;
        lineRend.SetPosition(1, point1);
    }
    private void ResetLaser()
    {
        start = transform.position;
        way = transform.forward;

    }
    public void OnTriggerStay(Collider other)
    {
        if (other.tag=="Player")
        {
            other.SendMessage("plusHP",damage);
        }
    }
}
