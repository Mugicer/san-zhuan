﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class puppet_charge1 : StateMachineBehaviour {
    private Transform player;
    private puppeteer puppeteer;
    private Vector3 v;
	 // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        player = animator.GetComponent<puppet>().player.transform;
        puppeteer = animator.GetComponent<puppet>().puppeteer;
        if (animator.GetComponent<puppet>().num== 0)
        {
            v= player.transform.position + puppeteer.transform.right * 2+puppeteer.transform.forward*2+new Vector3(0,1,0);
        }
        if (animator.GetComponent<puppet>().num == 1)
        {
            v = player.transform.position + puppeteer.transform.right * 2 * (-1) + puppeteer.transform.forward * 2 + new Vector3(0, 1, 0);
        }
        Debug.Log(v);

    }

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        if (animator.GetComponent<puppet>().num == 0)
        {
            animator.GetComponent<puppet>().target = v;
        }
        if (animator.GetComponent<puppet>().num == 1)
        {
            animator.GetComponent<puppet>().target = v;
        }
        bool a = animator.GetComponent<puppet>().Move();
        animator.GetComponent<puppet>().Look();
        if (a)
        {
            animator.SetBool("attack", true);
        }
    }

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        animator.SetBool("charge1",false);
	}

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
