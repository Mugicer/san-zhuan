﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoreData : MonoBehaviour {
    public static StoreData Instance;
    [Header("Player Settings")]
    public string Name = "Mugicer";
    [Range(0, 100)]
    public float xspeed = 60;
    [Range(0, 100)]
    public float yspeed = 30;
    [Range(0, 100)]
    public float CamaraLerp_move = 0.5f;
    [Range(0, 10)]
    public float range = 1;
    public float hop;

    [Header("Objtalk settings")]
    public float distence = 1;
    public float shootparameter = 1;

    [Header("Player Table")]
    public float LifeOfTime = 60;

    public float movespeed = 900;//移速
    public int str = 1;
    public int defence = 0;
    public int MaxHitPoint = 150;
    public int HitPoint = 150;
    public int MaxManaPoint = 100;
    public int ManaPoint = 100;
    [Header("Bag table")]
    [Header("Dash Parameter")]
    public int DashMp = 60;
    [Range(0, 100)]
    public float CamaraLerp_Dash = 0.5f;
    public float DashingTime;
    public float DashCamara_times;
    public float DashPower;
    [Header("God")]
    public bool eye = false;
    public bool women = false;
    public bool puppet = false;




    private void Awake()
    {
        if (Instance == null)
        {
            DontDestroyOnLoad(gameObject);
            Instance = this;
        }
        else if (Instance != null)
        {
            Destroy(gameObject);
        }
    }
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
