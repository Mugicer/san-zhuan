﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlindWomen_Showbullet : MonoBehaviour {
    public float speed = 5;
    private float p;
    void Start()
    {
        p=  Time.deltaTime * speed;
        Destroy(this.gameObject, 10);
    }
    
    void Update()
    {
        transform.Translate(0, 0, p * Time.timeScale);
    }
}
