﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss_BulletCreater : MonoBehaviour {
    public GameObject[] bullet;
    public int num = 2;
    public int now = 0;
    private GameObject copy;
    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (now < 0)
        {
            now = num - 1;
        }
        if (now > num - 1)
        {
            now = 0;
        }
        if (Input.GetKeyDown("h"))//測試按鈕
        {
            float a = Random.Range(-1f,1f);
            float b = Random.Range(-1f, 1f);
            Creat_ShadowBullet(a, b);
        }
    }
    void Creat_ShadowBullet(float x,float y)
    {
        copy = Instantiate(bullet[0],this.transform.position+new Vector3(x,y),this.transform.rotation);
    }
    void Creat_ShadowBullet(float x, float y,Quaternion ro)
    {
        copy = Instantiate(bullet[0], this.transform.position + new Vector3(x, y), ro);
    }

    void Creat_TrueBullet(float x, float y)
    {
        copy = Instantiate(bullet[1], this.transform.position + new Vector3(x, y), this.transform.rotation);
    }
    void Creat_TrueBullet(float x, float y, Quaternion ro)
    {
        copy = Instantiate(bullet[1], this.transform.position + new Vector3(x, y), ro);
    }
}
