﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserBoss : MonoBehaviour {
    public GameObject player;
    public GameObject HPbar;
    public Animator anim;
    public int HP = 999;
    public int MAXHP = 999;
    public GameObject treture;

    public GameObject bigeye;
    public GameObject[] Lasers;
    public GameObject[] taragets;
    public GameObject[] handeyes;
    public GameObject[] LaserOnWorld;
    public GameObject[] LaserTracing;
    public StoreData storedata;
    private Vector3 buset;
    private Quaternion rotationEuler;
    private Rigidbody rigid;
    void Awake(){
        player = GameObject.FindWithTag("Player");
        buset = bigeye.transform.position - transform.position;
        rigid = GetComponent<Rigidbody>();
        HPbar.GetComponent<Bar>().Max = MAXHP;
        HPbar.GetComponent<Bar>().now = HP;
        anim = transform.GetComponent<Animator>();
        storedata = GameObject.FindGameObjectWithTag("StoreData").GetComponent<StoreData>();
    }
    void Start () {
        if (StoreData.Instance.eye)
        {
            plusHP(-1*MAXHP);
        }
    }
    // Update is called once per frame
    void Update()
    {
        animParameter();
    }
    private void animParameter()
    {
        anim.SetFloat("hp", HP);
        HPbar.GetComponent<Bar>().now = HP;
        if (HP<0)
        {
            Dead();
        }
    }
    public void plusHP(float a)
    {
        HP = HP + (int)a;
    }
    public void Dead() {
        if (treture!=null)
        {
            treture.SetActive(true);
        }
        foreach (GameObject a in LaserOnWorld)
        {
            Destroy(a);
        }
        foreach(GameObject a in LaserTracing)
        {
            Destroy(a);
        }
        StoreData.Instance.eye = true;
        Destroy(this.gameObject);
    }

}
