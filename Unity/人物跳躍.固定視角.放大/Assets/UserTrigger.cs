﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserTrigger : MonoBehaviour {
    //本物體需要 
    //collider->trigger
    //rigidbody
    //指定objtalk物件 用來顯示文字的區塊 通常是消失的
    //被閱讀物件 給予標籤 readobj 具有文字
    public OBJtalk objtalk;
    public GameObject obj;
    public GameObject talkbtn;
    private player_input pi;
    // Use this for initialization
    private void Awake()
    {
        pi = GameObject.FindGameObjectWithTag("Player").GetComponent<player_input>();
    }
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {//持續偵測 有可閱讀 顯示scence 呼叫該可閱物件的方法 
        if (obj != null)
        {
            objtalk.targetOBJ = obj;
            if (obj.tag!="God")
            {
                //物件對話專用
                objtalk.SendMessage("Set_Meterial");
                if (objtalk.ObjInferrence.active == false)
                {
                    talkbtn.SetActive(true);

                    if (pi.talk)
                    {
                        objtalk.ObjInferrence.SetActive(true);
                        pi.talk = false;
                        talkbtn.SetActive(false);
                    }
                }

                if (pi.talk && objtalk.ObjInferrence.active == true)
                {
                    objtalk.ObjInferrence.SetActive(false);
                }
            }
            else//時間之神專用
            {
                if (obj.transform.GetComponent<TheGod>().talkplace.activeSelf==false)
                {
                    talkbtn.SetActive(true);

                    if (pi.talk)
                    {
                        obj.transform.GetComponent<TheGod>().askGod();
                        pi.talk = false;
                    }
                }
                else
                {
                    if (pi.talk)
                    {
                        obj.transform.GetComponent<TheGod>().btnQuit();
                        pi.talk = false;
                    }
                }
            }

        }
        else
        {
            talkbtn.SetActive(false);
        }
    }

    public void OnTriggerStay(Collider other) //穿透的物體有標籤[可閱讀物件] 獲取物件
    {
        if (other.tag == "readobj"||other.tag=="God")
        {
            obj = other.gameObject;
        }
    }
    public void OnTriggerExit(Collider other) //離開物件時  丟失物件
    {
        if (other.tag == "readobj")
        {

            objtalk.ObjInferrence.SetActive(false);
            obj = null;
        }
        if ( other.tag == "God")
        {
            obj.transform.GetComponent<TheGod>().btnQuit();
            obj = null;
        }
    }
}
