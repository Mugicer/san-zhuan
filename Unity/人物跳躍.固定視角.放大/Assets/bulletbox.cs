﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class bulletbox : MonoBehaviour {
    public Sprite[] images;
	// Use this for initialization
	void Start () {
        images = new Sprite[3];
	}
	
	// Update is called once per frame
	void Update () {
        if (images[1] != null)//中
        {
            transform.GetChild(3).GetComponent<Image>().sprite = images[1];

        }

        if (images[0]!=null)//左
        {
            transform.GetChild(0).GetComponent<Image>().sprite = images[0];
        }
        
        if (images[2] != null)//右
        {
            transform.GetChild(1).GetComponent<Image>().sprite = images[2];
        }
    }
}
