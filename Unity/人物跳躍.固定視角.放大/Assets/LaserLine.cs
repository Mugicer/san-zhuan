﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserLine : MonoBehaviour {
    public LineRenderer lineRenderer;
    public CapsuleCollider capsule;
    public float damage;
    public float LineWidth;
    public Vector3 point0;
    public Vector3 point1;
    bool use = true;
    float trigger_Time=0.5f;
    private RaycastHit Hit;
    // Use this for initialization
    private void Awake()
    {
        lineRenderer = GetComponent<LineRenderer>();
        capsule = GetComponent<CapsuleCollider>();
    }
    void Start()
    {
        LineWidth = lineRenderer.startWidth;
        capsule.radius = LineWidth / 2;
        capsule.center = Vector3.zero;
        Physics.Raycast(transform.position, transform.forward, out Hit);
        point0 = transform.position;
        point1 = Hit.point;
    }

    // Update is called once per frame
    void Update () {
        lineRenderer.SetPosition(0,point0);
        lineRenderer.SetPosition(1, point1);
    }
    public void setCapsule(Vector3 start,Vector3 end) {
        point0 = start;
     
        point1 = end;
        capsule.height = (point0 - point1).magnitude;//碰撞長度
        capsule.center = new Vector3(0,0, capsule.height/2);//碰撞中心
    }
    public void setLine(Vector3 start,Vector3 end) {
        point0=start;
        point1 = end;
    }
    public void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player" && use)
        {
            other.SendMessage("plusHP", damage);
            use = false;
            StartCoroutine(waitTime(trigger_Time));
        }

    }
    IEnumerator waitTime(float t)
    {
        yield return new WaitForSeconds(t);
        use = true;
    }
}
